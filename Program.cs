﻿﻿using Cairo;
using Gtk;
using Gdk;
using Color = Cairo.Color;
using static System.Console;
using static Gdk.EventMask;

public class DistanceComparer : IComparer<PointD>{
    PointD source = new PointD(0,0);
    public DistanceComparer(PointD inputSource){
        source = inputSource;
    }
    public int Compare(PointD a, PointD b){
        double getDistance(PointD p1, PointD p2){
            return Math.Sqrt(Math.Pow(p1.X - p2.X,2) + Math.Pow(p1.Y-p2.Y,2));
        }
        double distanceToA = getDistance(source, a);
        double distanceToB = getDistance(source, b);
        return distanceToA.CompareTo(distanceToB);

    }

}
class Area : DrawingArea{
    Color black = new Color(0, 0, 0),
          white = new Color(1, 1, 1),
          grey = new Color(0.5,0.5,0.5);
    List<Cairo.Rectangle> objects =  new List<Cairo.Rectangle>();
    double scalingFactor = 1;
    const double MIN_SCALING_FACTOR = 0.25;
    const double MAX_SCALING_FACTOR = 2;
    const double INCREMENT = 0.05;
    const double EXTRAPOLATE_SCALING_FACTOR = 50000;

    ConfigInfo config;
    public ConfigInfo settings{
        get{return config;}
    }
    (int X, int Y) upperLeftCorner;
    (int X, int Y) lowerRightCorner;
    PointD center;
    public Area(int sizeX, int sizeY){
        upperLeftCorner = (0, 0);
        lowerRightCorner = (sizeX, sizeY);
        config = new ConfigInfo();
        updateParameters(new ConfigInfo());
        QueueDraw();
        AddEvents((int) (PointerMotionMask|ScrollMask));
    }
    public void updateParameters(ConfigInfo info){
        config = info;
        generateObjects();
    }
    public void generateObjects(){
        List<Cairo.Rectangle> obj =  new List<Cairo.Rectangle>();
        for (int i = 0; i < config.numOfObjects; ++i){
            obj.Add(generateRectangle());
        }
        objects = obj;
    }
    Cairo.Rectangle generateRectangle(){
        Random r = new Random();
        int width = r.Next(config.minimumWidth,config.maximumWidth+1);
        int height = r.Next(config.minimumHeight,config.maximumHeight+1);
        int X = r.Next(upperLeftCorner.X,lowerRightCorner.X + 1);
        int Y = r.Next(upperLeftCorner.Y,lowerRightCorner.Y + 1);
        return new Cairo.Rectangle(x: X, y: Y, width: width, height: height);
    }
    bool isInsideObject(PointD p){
        foreach (var obj in objects){
            if (p.X >= obj.X && p.X <= obj.X + obj.Width &&
                p.Y >= obj.Y && p.Y <= obj.Y + obj.Height){
                    return true;
                }
        }
        return false;
    }
    List <PointD> getShadowPivots(PointD source, Cairo.Rectangle r){
        /*
        Input : PointD instance of light source
                Cairo.Rectangle object whose shadows we are considering
        Output : A List of two Points
        */
        List<PointD> pivots = new List<PointD>();
        double leftX = r.X;
        double rightX = r.X + r.Width;
        double leftY = r.Y;
        double rightY = r.Y + r.Height;
        
        if (source.X >= leftX && source.X <= rightX){
            double p_y = Math.Abs(source.Y - leftY) < Math.Abs(source.Y - rightY) ?
                         leftY : rightY;
            pivots.Add(new PointD(leftX,p_y));
            pivots.Add(new PointD(rightX,p_y));
        }
        else if (source.Y >= leftY && source.Y <= rightY){
            double p_x = Math.Abs(source.X - leftX) < Math.Abs(source.X - rightX) ?
                         leftX : rightX;
            pivots.Add(new PointD(p_x, leftY));
            pivots.Add(new PointD(p_x, rightY));
        }
        else{
            pivots.Add(new PointD(leftX,leftY));
            pivots.Add(new PointD(leftX,rightY));
            pivots.Add(new PointD(rightX,leftY));
            pivots.Add(new PointD(rightX,rightY));
            DistanceComparer dc = new DistanceComparer(center);
            pivots.Sort(dc);
            pivots.RemoveAt(3);
            pivots.RemoveAt(0);
        }
        return pivots;
    }
    PointD extrapolateLine(PointD a, PointD b){
        double len = Math.Sqrt(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        double unit_x = (b.X - a.X) / len, unit_y = (b.Y - a.Y) / len;
        return new PointD(b.X + EXTRAPOLATE_SCALING_FACTOR * unit_x,
                            b.Y + EXTRAPOLATE_SCALING_FACTOR * unit_y);
    }
    void draw(Context c){
        //Draw the background
        c.SetSourceColor(black);
        c.Paint();
        
        PointD s = new PointD(center.X, center.Y);
        if (!isInsideObject(s)){
            //Draw the light source and uncut light radiation
            RadialGradient R = new RadialGradient(cx0: center.X, cy0: center.Y,
                                                radius0: scalingFactor *5,
                                                cx1: center.X, cy1: center.Y, 
                                                radius1: scalingFactor*200);
            R.AddColorStop(0.0, white);
            R.AddColorStop(1.0, black);

            c.SetSource(R);
            c.Arc(xc: center.X, yc: center.Y, radius: scalingFactor*200, angle1: 0, angle2: 2 * Math.PI);
            c.Fill();
            
            //Cut out shadows
            c.SetSourceColor(black);
            c.LineWidth = 0;
            
            foreach (var obj in objects){
                List<PointD> p = getShadowPivots(s, obj);
                PointD extrapolate1 = extrapolateLine(s ,p[0]);
                PointD extrapolate2 = extrapolateLine(s ,p[1]);
                c.MoveTo(p[0].X,p[0].Y);
                c.LineTo(extrapolate1.X,extrapolate1.Y);
                c.LineTo(extrapolate2.X,extrapolate2.Y);
                c.LineTo(p[1].X,p[1].Y);
                c.ClosePath();
                c.StrokePreserve();   
                c.SetSourceColor(black);
                c.Fill();
        }
        }

        //Draw the objects
        c.SetSourceColor(grey);
        foreach (var r in objects){
            c.Rectangle(x: r.X, y: r.Y, width: r.Width, height: r.Height);
            c.Fill();
        }
    }
    protected override bool OnDrawn (Context c) {
        draw(c);
        return true;
    }
    protected override bool OnMotionNotifyEvent(EventMotion e) {
        center.X = e.X;
        center.Y = e.Y;
        QueueDraw();
        return true;
    }
    protected override bool OnScrollEvent(EventScroll e){
        int dir = e.Direction == ScrollDirection.Up ? 1 : -1;
        scalingFactor = scalingFactor + (dir * INCREMENT);
        scalingFactor = Math.Max(scalingFactor, MIN_SCALING_FACTOR);
        scalingFactor = Math.Min(scalingFactor, MAX_SCALING_FACTOR);
        QueueDraw();
        return true;
    }
}
record ConfigInfo(int minimumWidth = 25, int maximumWidth = 300 ,
                  int minimumHeight = 25, int maximumHeight = 300, 
                  int numOfObjects = 5);
class ConfigDialog : Gtk.Dialog{
    Scale minimumWidthSlider;
    Scale maximumWidthSlider;
    Scale minimumHeightSlider;
    Scale maximumHeightSlider;
    Scale numOfObjectsSlider;
    public ConfigDialog(Gtk.Window parent, ConfigInfo original): 
                    base("Config Mode", parent, DialogFlags.Modal,
                   "Generate objects", ResponseType.Ok, "Cancel", ResponseType.Cancel){
        Box box = new Box(Orientation.Vertical,0);

        box.PackStart(new Label("Minimum Width"),true,true,0);
        minimumWidthSlider = new Scale(Orientation.Horizontal, 25, 300 ,5);
        minimumWidthSlider.Value = original.minimumWidth;
        minimumWidthSlider.ValueChanged += onChangeWidth;
        box.PackStart(minimumWidthSlider, true, true,0);

        box.PackStart(new Label("Maximum Width"),true,true,0);
        maximumWidthSlider = new Scale(Orientation.Horizontal, 25, 300 ,5);
        maximumWidthSlider.Value = original.maximumWidth;
        maximumWidthSlider.ValueChanged += onChangeWidth;
        box.PackStart(maximumWidthSlider, true, true,0);

        box.PackStart(new Label("Minimum Height"),true,true,0);
        minimumHeightSlider = new Scale(Orientation.Horizontal, 25, 300 ,5);
        minimumHeightSlider.Value = original.minimumHeight;
        minimumHeightSlider.ValueChanged += onChangeHeight;
        box.PackStart(minimumHeightSlider, true, true,0);

        box.PackStart(new Label("Maximum Height"),true,true,0);
        maximumHeightSlider = new Scale(Orientation.Horizontal, 25, 300 ,5);
        maximumHeightSlider.Value = original.maximumHeight;
        maximumHeightSlider.ValueChanged += onChangeHeight;
        box.PackStart(maximumHeightSlider, true, true,0);

        box.PackStart(new Label("Number of Objects"),true,true,0);
        numOfObjectsSlider = new Scale(Orientation.Horizontal,1, 10, 1);
        numOfObjectsSlider.Value = original.numOfObjects;
        box.PackStart(numOfObjectsSlider, true, true,0);
        ContentArea.Add(box);
        ShowAll();
    }
    void onChangeWidth(object? obj, EventArgs args){
        maximumWidthSlider.Value = Math.Max(maximumWidthSlider.Value, minimumWidthSlider.Value);
        minimumWidthSlider.Value = Math.Min(maximumWidthSlider.Value, minimumWidthSlider.Value);
        
    }
    void onChangeHeight(object? obj, EventArgs args){
        maximumHeightSlider.Value = Math.Max(maximumHeightSlider.Value, minimumHeightSlider.Value);
        minimumHeightSlider.Value = Math.Min(maximumHeightSlider.Value, minimumHeightSlider.Value);
        
    }
     public ConfigInfo getConfigInfo() {
        return new ConfigInfo((int)minimumHeightSlider.Value,
                            (int)maximumHeightSlider.Value,
                            (int)minimumWidthSlider.Value,
                            (int)maximumWidthSlider.Value,
                            (int)numOfObjectsSlider.Value);
    }
}
class MainWindow : Gtk.Window{
    Area area;
    Box box;
    Button button;
    int WIDTH = 600;
    int HEIGHT = 600;
    public MainWindow() : base("2D Lighting System"){
        Resize(WIDTH,HEIGHT);
        box = new Box(Orientation.Vertical, 5);
        button = new Button("Set Parameters");
        button.Clicked += OnClick;
        area = new Area(WIDTH,HEIGHT);
        
        box.PackStart(button,false,false,0);
        box.PackStart(area,true,true,0);
        Add(box);
    }
    void OnClick(object? sender, EventArgs args){
        ConfigMode();
    }
    void ConfigMode(){
        using (ConfigDialog d = new ConfigDialog(this, area.settings)){
            if (d.Run() == (int) ResponseType.Ok){
                ConfigInfo newConfig = d.getConfigInfo();
                area.updateParameters(newConfig);
            }
        }
    }
    protected override bool OnDeleteEvent(Event e) {
        Application.Quit();
        return true;
    }
}

class Wrapper{
    static void Main(){
        Application.Init();
        MainWindow w = new MainWindow();
        w.ShowAll();
        Application.Run();
    }
}
