# 2D Lighting System

2D Top-down simulation of the shadows that a light source casts over randomly generated objects. The mouse cursor controls the movement of the light source.

## Getting Started

### Prerequisites

GtkSharp

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thank you to Professor Adam Dingle for providing constructive feedback and helpful advice on the project multiple times.
